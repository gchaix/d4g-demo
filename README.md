# Drupal 8 Composer scaffold with drush and coder module sniffing

## Usage
1. `composer install`
2. `ddev config`
3. `ddev start`
4. https://d4g-demo.ddev.site
